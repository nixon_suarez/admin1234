import Head from 'next/head'
import Banner from '../components/Banner'

export default function Home() {
  const data = {
    title: "Circle",
    content: "Manage ",
    destination: "/register",
    label: "Enroll now!"
  }
  
  return (
    <React.Fragment>
      <Banner dataProp={data}/>
    </React.Fragment>
  )
}